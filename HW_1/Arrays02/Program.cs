﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysBubble
{
    public class Program
    {
        static void Main(string[] args)
        {
            int[] array = ArrayTools.CreateRandomArray(5);
            ArrayTools.DisplayArray(array);

            ArraySort sort = new ArraySort();
            sort.BubbleSort(array);
            ArrayTools.DisplayArray(array);

            Console.ReadKey();

        }
    }
}
