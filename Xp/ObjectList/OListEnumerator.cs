﻿using System.Collections;

namespace ObjectList
{
    internal class OListEnumerator : IEnumerator
    {
        private object[] container;
        private int count;

        public OListEnumerator(object[] container, int count)
        {
            this.container = container;
            this.count = count;
        }
        private int position = -1;
        public bool MoveNext()
        {
            if (position < count - 1)
            {
                position++;
                return true;
            }
            else
            {
                return false;
            }
        }
        public object Current => container[position];


        public void Reset()

        {
            position = -1;
        }
    }
}