﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchBreak
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 1;
            int b = 3;
            var rand = new Random();

            for (int i = 0; i < 10; i++)
            {
                var x = rand.Next(0, 3);
                switch (x) 
                {
                    case 1://перейдет к метке 3
                    //case 2: return;//выйдет из программы
                    case 3:
                        Console.WriteLine(a+b);
                        continue;// сработает как и break - пропустит текущую итерацию
                    default: Console.WriteLine("def");
                    break;
                }
            }

            Console.ReadLine();//и вообще весь switch это if, нас обманули, расходимся
        }
    }
}
