﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankBinary
{
    class Solution
    {

        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int max = 0;
            int counter = 0;
            int previousDigit = 0;
            int currentDigit = 0;
            while (n != 0)
            {
                previousDigit = currentDigit;
                if ((n & 1) == 1)
                {
                    previousDigit = currentDigit;
                    currentDigit = 1;
                }
                else
                {
                    currentDigit = 0;
                }

                if ((currentDigit == 1) && (previousDigit == 1))
                {
                    counter++;
                }

                if ((currentDigit == 1) ^ (previousDigit == 1))
                {
                    counter = 1;
                }

                if (max < counter)
                {
                    max = counter;
                }

                n = n >> 1;
            }
            Console.WriteLine(max);
        }
    }
}
