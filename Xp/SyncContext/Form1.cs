﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncContext
{
    public partial class Form1 : Form
    {

        private delegate void EnterTextDelegate();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var del = new EnterTextDelegate(EnterText);
            del.BeginInvoke(null,null);
        }

        private void EnterText()
        {
            textBox1.Text = "fail";
        }
    }
}
