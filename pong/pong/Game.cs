﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pong
{
    public partial class Game : Form
    {
        private Player player1, player2;
        private Ball ball;
        private Point topOfWorld, bottomOfWorld, startPosition;

        public Game()
        {
            InitializeComponent();
            topOfWorld = new Point(0, 0);
            bottomOfWorld = new Point(this.ClientSize.Width, this.ClientSize.Height);
            startPosition = new Point((topOfWorld.X + bottomOfWorld.X) / 2, (topOfWorld.Y + bottomOfWorld.Y) / 2);
            player1 = new Player(paddleObject1, labelPlayerOneObject, topOfWorld, bottomOfWorld);
            player2 = new Player(paddleObject2, labelPlayerTwoObject, topOfWorld, bottomOfWorld);
            ball = new Ball(ballObject, topOfWorld, bottomOfWorld, startPosition, player1, player2);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            player1.ProcessMove();
            player2.ProcessMove();
            ball.ProcessMove();

        }
        //Disable keys
        private void Game_KeyUp(object sender, KeyEventArgs e)
        {
            var isPressed = false;
            CheckKeys(e, isPressed);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        //Check if one of keys is pressed
        private void Game_KeyDown(object sender, KeyEventArgs e)
        {
            var isPressed = true;
            CheckKeys(e, isPressed);
        }

        private void CheckKeys(KeyEventArgs e, bool isPressed)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                    player1.isUpPressed = isPressed;
                    break;
                case Keys.S:
                    player1.isDownPressed = isPressed;
                    break;
                case Keys.Up:
                    player2.isUpPressed = isPressed;
                    break;
                case Keys.Down:
                    player2.isDownPressed = isPressed;
                    break;
            }
        }
    }

}
