﻿using System;
using System.Threading;

namespace ThreadParams
{
    class Program
    {
        static void Main(string[] args)
        {
            Counter counter = new Counter();
            counter.x = 4;
            counter.y = 5;

            Thread myThread = new Thread(new ParameterizedThreadStart(Count));
            Console.WriteLine("Thread created");
            myThread.Start(counter);
            Console.ReadLine();

            //...................
        }

        public static void Count(object obj)
        {
            for (int i = 1; i < 9; i++)
            {
                Counter c = (Counter)obj;

                Console.WriteLine("Second Thread:");
                Console.WriteLine(i * c.x * c.y);
            }
        }
    }

    public class Counter
    {
        public int x;
        public int y;
    }
}
