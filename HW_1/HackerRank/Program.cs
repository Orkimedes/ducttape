﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank
{
    class Solution
    {
        

        static void Main(String[] args)
        {
            int i = int.Parse(Console.ReadLine());
            string[] bodies = new string[i];
            Dictionary<string, int> dic = PhoneBook.CreateDictionary(i);
            for (int j = 0; j < i; j++)
            {
                bodies[j] = Console.ReadLine();
            }

            for (int j = 0; j < i; j++)
            {
                PhoneBook.FindElement(dic, bodies[j]);
            }
        }
    }

    class PhoneBook
    {
        public static Dictionary<string, int> CreateDictionary(int i)
        {
            Dictionary<string, int> dic = new Dictionary<string, int>();
            for (int j = 0; j < i; j++)
            {
                string name;
                int number;
                string[] arr_temp = Console.ReadLine().Split(' ');
                name = arr_temp[0];
                number = Int32.Parse(arr_temp[1]);
                dic.Add(name, number);
            }
            return dic;
        }

        public static void TypeElements(Dictionary<string, int> dic)
        {
            foreach (string c in dic.Keys)
            {
                Console.WriteLine(c+"="+dic[c]);
            }
        }

        public static void FindElement(Dictionary<string, int> dic, string key)
        {
            if (dic.ContainsKey(key))
            {
                Console.WriteLine(key + "=" + dic[key]);
            }
            else
            {
                Console.WriteLine("Not found");
            }
        }


    }

}
