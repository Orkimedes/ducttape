﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectList
{
    class Program
    {
        static void Main(string[] args)
        {
            var aList = new ArrayList();
            int a = 5;
            int b = 10;
            aList.Add(a);
            aList.Add(b);
            Console.WriteLine((int)aList[0] + (int)aList[1]);

            Console.WriteLine();
            var objectList = new OList();
            int position = objectList.Add(5);
            Console.WriteLine(position);
            Console.WriteLine(objectList[position]);
            objectList.Add(10);
            foreach (var item in objectList)
            {
                Console.WriteLine(item);
            }
            //Console.WriteLine(objectList[0] + objectList[1]); - не сработает, так как тип этих объектов - object
            
            Console.ReadLine();
        }
    }
}
