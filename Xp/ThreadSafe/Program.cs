﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadSafe
{
    class ThreadSafe
{
  static List<string> list = new List<string>();

    static void Main()
    {
        new Thread(AddItems).Start();
        new Thread(AddItems).Start();
    }

    static void AddItems()
    {
        for (int i = 0; i < 100; i++)
            lock (list)
                Add("Item " + list.Count);

        string[] items;

        lock (list)
            items = list.ToArray();

        foreach (string s in items)
            Console.WriteLine(s);
    }
}
}
