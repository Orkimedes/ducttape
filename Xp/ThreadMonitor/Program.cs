﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadMonitor
{
    class Program
    {
        static object locker = new object();
        static void Main()
        {
            Thread writeOneThread = new Thread(() => WriteOneOrTwoFiveTimes("One"));
            Thread writeTwoThread = new Thread(() => WriteOneOrTwoFiveTimes("Two"));


            writeOneThread.Start();
            writeTwoThread.Start();
            writeOneThread.Join();
            writeTwoThread.Join();


            Console.WriteLine("Well done!");
            Console.ReadLine();
        }
        static void WriteOne(bool isRunning)
        {
            lock (locker)
            {
                if (!isRunning)
                {
                    Monitor.Pulse(locker);  /*Снимаем блокировку с локера*/
                    return;                 /*завершаем работу метода*/
                }


                Console.Write("One - ");


                Monitor.Pulse(locker);      /*Снимаем блокировку с локера*/
                Monitor.Wait(locker);       /*Останавливаем работу потока и ожидаем снятия блокировки с локера (сигнала от Monitor.Pulse(locker) вызваного другим потоком)*/
            }
        }
        static void WriteTwo(bool isRunning)
        {
            lock (locker)
            {
                if (!isRunning)
                {
                    Monitor.Pulse(locker);
                    return;
                }
                Console.WriteLine("Two");
                Monitor.Pulse(locker);
                Monitor.Wait(locker);
            }
        }
        static void WriteOneOrTwoFiveTimes(string oneOrTwo)
        {
            if (oneOrTwo == "One")
            {
                for (int i = 0; i < 5; i++)
                    WriteOne(true);    /*Вызываем метод 5 раз*/
                WriteOne(false);      /*Вызываем для разблокировки lockerа и завершения работы*/
            }
            else
            {
                for (int i = 0; i < 5; i++)
                    WriteTwo(true);
                WriteTwo(false);
            }
        }
    }
}
