﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fields
{
    class Program
    {
        static void Main(string[] args)
        {
            float acceleration = MyVariables.AOG;//доступ есть изначально
            Console.WriteLine($"acceleration = {acceleration}");
            float density = MyVariables.Density;// = 1.25, вызвался статический конструктор
            //float speed = MyVariables.MySpeed; - нет доступа так как объекта нет
            Console.WriteLine($"Density = {density}");
            var xVar = new MyVariables(25);
            xVar.Display();
            xVar.ShowMeTrueSpeed();
            var yVar = new MyVariables();
            yVar.Display();
            var zVar = new MyVariables(10);
            zVar.Display();
            Console.ReadLine();

        }
    }

    class MyVariables
    {
        //public const float AOG; - должна быть инициализирована при объявлении
        public const float AOG = 9.8F;
        public readonly int mySpeed = 1;// можно инициализировать конструктором
        public static readonly float Density;//можно инициализировать статическим конструктором

        static MyVariables()//нет модификаторов, нет параметров, нельзя обратится к нестатитеским членам
        {
            Console.WriteLine("static ctor");
            Density = 1.25F;
        }
        public MyVariables()
        {
            Console.WriteLine("def instance ctor");
        }

        public MyVariables(int mySpeed)
        {
            Console.WriteLine("instance ctor");
            this.mySpeed = mySpeed;//задаем значение поля текущему экземпляру класса
        }
        public void Display()
        {
            Console.WriteLine(AOG);
            Console.WriteLine(mySpeed);
            Console.WriteLine(Density);
        }
        public void ShowMeTrueSpeed()
        {
            //static - нет
            //readonly - нет
            const long speedOfLight = 3000000000;
            Console.WriteLine(speedOfLight + "!!!");
        }

    }
    public static class NotDynamic
    {
        const int MEANINGOFEVERYTHING = 42;
    }
    readonly struct User { }

}
