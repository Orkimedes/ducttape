﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lock
{
    public partial class MainScreen : Form
    {
        const int CODELENGTH = 3;
        private CheckBox[] controlPanelButtons;
        private enum Status
        {
            locked,
            unlocked,
            freeOfUse
        }
        private Status currentStatus;
        private CheckBox[] currentCode = new CheckBox[CODELENGTH];
        public MainScreen()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            labelStatus.Text = null;
            InitializeControlButtons();
            currentStatus = Status.freeOfUse;
        }

        private void InitializeControlButtons()
        {
            controlPanelButtons = new CheckBox[] {checkBox0,checkBox1,checkBox2,checkBox3,checkBox4,checkBox5,checkBox6,checkBox7,checkBox8,checkBox9};
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            foreach (CheckBox checkbox in controlPanelButtons)
                checkbox.Checked = false;
        }
         
        private int CalculateActiveButtons(CheckBox[] array)
        {
            int count = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i].Checked == true)
                {
                    count++;
                }
            }
            infoBox.Text = count.ToString();
            return count;
        }

        
    }
}
