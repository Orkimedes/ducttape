﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Получить строковое название дня недели по номеру дня. 

namespace Methods01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter number of a day:");
            int day = Int32.Parse(Console.ReadLine());
            Console.WriteLine($"This day is {ConvertToDayName(day)}");

            Console.ReadKey();
        }

        static string ConvertToDayName(int day)
        {
            string dayName = "Unknown";
            switch (day)
            {
                case 1:
                    dayName = "Monday";
                    break;
                case 2:
                    dayName = "Tuesday";
                    break;
                case 3:
                    dayName = "Wednesday";
                    break;
                case 4:
                    dayName = "Thusrday";
                    break;
                case 5:
                    dayName = "Friday";
                    break;
                case 6:
                    dayName = "Saturday";
                    break;
                case 7:
                    dayName = "Sunday";
                    break;
                default:
                    break;
            }

            return dayName;
        }
    }
}
