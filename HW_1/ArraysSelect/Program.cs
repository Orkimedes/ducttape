﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysSelect
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[] array = ArrayTools.CreateRandomArray(5);
            ArrayTools.DisplayArray(array);

            ArraySort sort = new ArraySort();
            sort.SelectSort(array);
            ArrayTools.DisplayArray(array);

            Console.ReadKey();

        }
    }
}
