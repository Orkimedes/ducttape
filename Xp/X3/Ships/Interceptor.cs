﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X3
{
    class Interceptor : Ship, IBattleship, IWingman
    {
        public int SerialNuber { get; }
        public Interceptor(int serialNumber)
        {
            SerialNuber = serialNumber;
        }

        public void Attack()
        {
            Console.WriteLine("-300HP!!!"); ;
        }

        public override void Move()
        {
            Console.WriteLine("Interceptor moving");
        }
    }
}
