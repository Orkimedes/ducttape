﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X3
{
    
    public abstract class Ship//- класс объявлен как абстрактный так как есть абстрактные методы, кроме того создаваться объект типа Ship не должен.
    {
        public int MaxArmor { get; }
        public int MaxShieldCapacity { get; }
        public int MaxCargo { get; }
        public int MaxSpeed { get; }

        protected string name;
        public Ship(int maxArmor, int maxShieldCapacity, int maxCargo, int maxSpeed)
        {
            MaxArmor = maxArmor;
            MaxShieldCapacity = maxShieldCapacity;
            MaxCargo = maxCargo;
            MaxSpeed = maxSpeed;
        }

        protected Ship()//del
        {
        }

        public abstract void Move();
        public virtual void Stop()
        {
            Console.WriteLine("Stop");
        }


    }
}
