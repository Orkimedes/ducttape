﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankSorting
{
    class Solution
    {

        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] a_temp = Console.ReadLine().Split(' ');
            int[] a = Array.ConvertAll(a_temp, Int32.Parse);
            ArraySort(a);
            // Write Your Code Here
        }

        static void ArraySort(int[] myArray)
        {
            int totalNumberOfSwaps = 0;
            for (int i = 0 , n = myArray.Length; i < n; i++)
            {
                // Track number of elements swapped during a single array traversal
                int numberOfSwaps = 0;
                

                for (int j = 0; j < n - 1; j++)
                {
                    // Swap adjacent elements if they are in decreasing order
                    if (myArray[j] > myArray[j + 1])
                    {
                        Swap(ref myArray[j], ref myArray[j + 1]);
                        numberOfSwaps++;
                    }
                }

                // If no elements were swapped during a traversal, array is sorted
                if (numberOfSwaps == 0)
                {
                    break;
                }
                else
                {
                    totalNumberOfSwaps += numberOfSwaps;
                }
            }
            Console.WriteLine($"Array is sorted in {totalNumberOfSwaps} swaps.");
            Console.WriteLine($"First Element: {myArray[0]}");
            Console.WriteLine($"Last Element: {myArray[myArray.Length - 1]}");
        }

        static void Swap(ref int a, ref int b)
        {
            int temp = a;
            a = b;
            b = temp;
        }
    }
}
