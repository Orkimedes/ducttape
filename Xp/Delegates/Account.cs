﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegates
{
    class Account
    {
        int _sum; // Переменная для хранения суммы

        public delegate void AccountStateHandler(string message);
        AccountStateHandler _del;
        public void RegisterHandler(AccountStateHandler del)
        {
            _del = del;
        }
        public void UnregisterHandler(AccountStateHandler del)
        {
            _del -= del; // удаляем делегат
        }
        public Account(int sum)
        {
            _sum = sum;
        }

        public int CurrentSum
        {
            get { return _sum; }
        }

        public void Put(int sum)
        {
            _sum += sum;
        }

        public void Withdraw(int sum)
        {
            if (sum <= _sum)
            {
                _sum -= sum;
                if(_del != null)
                {
                    _del($"Cash {sum} stolen");
                }

            }
            else
            {
                if (_del!=null)
                {
                    _del("You've not enough minerals");
                }
            }
        }
    }
}
