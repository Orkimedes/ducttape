﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Проверить простое ли число? (число называется простым, если оно делится только само на себя и на 1)

namespace Cycles02
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;
            Console.Write("Enter the number: ");
            number = Int32.Parse(Console.ReadLine());
            if (IsPrimeNumber(number))
            {
                Console.WriteLine($"{number} is a prime number!");
            }
            else
            {
                Console.WriteLine($"{number} is not a prime number");
            }

            Console.ReadKey();
        }

        static bool IsPrimeNumber(int number)
        {
            int counter = 0;
            for (int i = 1; i <= number; i++)
            {
                if (number%i == 0)
                {
                    counter++;
                }
            }

            if (counter == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
