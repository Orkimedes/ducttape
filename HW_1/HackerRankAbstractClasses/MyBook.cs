﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankAbstractClasses
{
    class MyBook:Book
    {
        public MyBook(String t, String a, int p) :base(t,a)
        {
            price = p;
        }
        private int price;

        public override void display()
        {
            Console.WriteLine($"Title: {title}");
            Console.WriteLine($"Author: {author}");
            Console.WriteLine($"Price: {price}");
        }

    }
}
