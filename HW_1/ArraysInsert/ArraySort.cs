﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysInsert
{
    public class ArraySort
    {
        public void InsertSort(int[] array)
        {
            if (array == null)
            {
                throw new NullReferenceException();
            }
            if (array.Length <= 1)
            {
                return;
            }

            for (int i = 1; i < array.Length; i++)
            {
                int temp = array[i];
                int j = i - 1;

                while (j >= 0 && array[j] > temp)
                {
                    array[j + 1] = array[j];
                    j--;
                }
                array[j + 1] = temp;
            }
            
        }
    }
}
