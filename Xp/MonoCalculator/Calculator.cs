﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCalculator
{
    class Calculator
    {
        private string expression;
        private Calculator innerCalculator;
        private string[] operations = { "*", "/", "+", "-" };
        public Calculator(string expression)
        {
            this.expression = expression;
        }
        public double PerformOperation(string op, double x, double y)
        {
            return OperationDic[op](x, y);
        }
        private Dictionary<string, Func<double, double, double>> OperationDic = new Dictionary<string, Func<double, double, double>>
        {
            { "*",  (x, y) => x * y},
            { "+", (x, y) => x + y },
            { "-", (x, y) => x - y },
            { "/", (x, y) => x / y },
        };
        public double Parse()
        {
            int startIndex;
            int endIndex;
            while ((endIndex = expression.IndexOf(')'))!=-1)
            {
                startIndex = expression.LastIndexOf('(',endIndex);
                innerCalculator = new Calculator(expression.Substring(startIndex+1, endIndex - startIndex -1));
                expression = expression.Remove(startIndex, endIndex - startIndex + 1);
                expression = expression.Insert(startIndex, innerCalculator.Parse().ToString());
            }

            for (int i = 0; i < operations.Length; i++)
            {
                while(SearchOperation(operations[i]) != -1)
                {
                    int currentPosition = SearchOperation(operations[i]);
                    SolveSingleOperation(operations[i], currentPosition);
                }
            }
            return Double.Parse(expression);
        }
        
        private void SolveSingleOperation(string currentOperation, int currentPosition)
        {
            double a = SearchLeftArguement(currentPosition, out int startOperationPosition);
            double b = SearchRightArguement(currentPosition, out int endOperationPosition);
            string operation = a + currentOperation + b;
            double operationResult = PerformOperation(currentOperation, a, b);
            expression = expression.Remove(startOperationPosition, endOperationPosition - startOperationPosition + 1);
            expression = expression.Insert(startOperationPosition, operationResult.ToString());
        }

        private int SearchOperation(string op)
        {
            int searchResult = expression.IndexOf(op);
            return searchResult;
        }

        private double SearchLeftArguement(int position, out int startArguementPosition)
        {
            string arguement = null;
            startArguementPosition = 0;
            for (int i = position-1; i>=0; i--)
            {
                if (expression[i].Equals('.')|| Char.IsDigit(expression[i]))
                {
                    arguement = expression[i] + arguement;
                }
                else
                {
                    startArguementPosition = i + 1;
                    break;
                }
            }
            return Double.Parse(arguement);
        }
        private double SearchRightArguement(int position, out int endOperationPosition)
        {
            string arguement = null;
            endOperationPosition = 0;
            for (int i = position + 1; i < expression.Length; i++)
            {
                if (expression[i].Equals('.') || Char.IsDigit(expression[i]))
                {
                    arguement = arguement + expression[i];
                    endOperationPosition = i;
                }
                else
                {
                    endOperationPosition = i-1;
                    break;
                }
            }
            return Double.Parse(arguement);
        }
        private bool CheckSpelling()
        {
            int countOpen = 0;
            int countClose = 0;
            for (int i = 0; i < expression.Length; i++)
            {
                if (expression[i] == '(')
                {
                    countOpen++;
                }
                if (expression[i] == ')')
                {
                    countClose++;
                }
            }
            if (countClose == countOpen)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /*private bool SolveOperation(string op1, string op2)
        {
            string currentOperation;
            int currentPosition;
            double a;
            double b;
            int positionOp1 = expression.IndexOf(op1);
            int positionOp2 = expression.IndexOf(op2);

            if (positionOp1 == positionOp2)
            {
                return false; 
            }
            else if (positionOp1 == -1)
            {
                currentOperation = op2;
                currentPosition = positionOp2;
            }
            else if (positionOp2 ==-1)
            {
                currentOperation = op1;
                currentPosition = positionOp1;
            }
            else if (positionOp1 < positionOp2)
            {
                currentOperation = op1;
                currentPosition = positionOp1;
            }
            else
            {
                currentOperation = op2;
                currentPosition = positionOp2;
            }
            a = SearchLeftArguement(currentPosition, out int startOperationPosition);
            b = SearchRightArguement(currentPosition, out int endOperationPosition);
            string operation = a + currentOperation + b;
            double operationResult = PerformOperation(currentOperation, a, b);
            expression = expression.Remove(startOperationPosition, endOperationPosition-startOperationPosition+1);
            expression = expression.Insert(startOperationPosition, operationResult.ToString());
            return true;
        }*/

        /*public double PerformOperation(IOperator op, double x, double y)
        {
            switch (op.Type)
            {
                case '+': return Add(x, y);
                case '-': return Substract(x, y);
                case '*': return Multiply(x, y);
                case '/': return Divide(x, y);
                default: throw new Exception();
            }
        }
        private double Divide(double x, double y)
        {
            if (y == 0)
            {
                throw new Exception("Division by zero");
            }
            else
            {
                return x / y;
            }
           
        }
        private double Multiply(double x, double y)
        {
            return x * y;
        }
        private double Substract(double x, double y)
        {
            return x - y;
        }
        private double Add(double x, double y)
        {
            return x + y;
        }*/




    }
}
