﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Найти сумму четных чисел и их количество в диапазоне от 1 до 99

namespace Cycles01
{
    class Program
    {
        static void Main(string[] args)
        {
            int start = 1;
            int end = 99;
            int sum = CalculateSum(start, end);
            Console.WriteLine($"Sum of numbers from {start} to {end} = {sum}");
            Console.WriteLine($"Number of numbers from {start} to {end} = {sum}");
            Console.ReadKey();
        }

        static int CalculateSum(int a, int b)
        {
            int result = 0;
            for (int i = a; i <= b; i++)
            {
                if (i%2 == 0)
                {
                    result += i;
                }
            }
            return result;
        }

        static int CalculateNumber(int a, int b)
        {
            int result = 0;
            for (int i = a; i <= b; i++)
            {
                if (i % 2 == 0)
                {
                    result++;
                }
            }
            return result;
        }
    }
}
