﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var Calc = new Overload();
            Console.WriteLine(Calc.SumOfVariables(1,2,3,4));
            Console.ReadLine();
        }
    }

    class Overload
    {
        public int SumOfVariables(int a, int b)
        {
            Console.WriteLine("1st");
            return a + b;
        }

        /*public int SumOfVariables(int a, int b)
        {
            return a + 2*b;
        }*/

        /*public double SumOfVariables(int a, int b)
        {
            return a + b;
        }*/

        public double SumOfVariables(double a, double b)
        {
            Console.WriteLine("2nd");
            return a + b;
        }

        public int SumOfVariables(int a, int b,  int c = 2)
        {
            Console.WriteLine("3rd");
            return a + b + c;
        }
        public int SumOfVariables(int a, int b, params int[] arr)
        {
            Console.WriteLine("4th");
            int c = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                c += arr[i];
            }
            return a + b + c;
        }

        public int SumOfVariables(int a, int b, out int c)
        {
            Console.WriteLine("5th");
            c = a*b;
            return a + b + c;
        }



        public int SumOfVariables(int a, ref int b)
        {
            Console.WriteLine("6th");
            b++;
            return a + b;
        }

        public int SumOfVariables(int a, int b, int c, int d)
        {
            Console.WriteLine("7th");
            return a + b + c + d;
        }

        /*public int SumOfVariables(int a, out int b)
        {
            b = a*2;
            return a + b;
        }*/



    }

}
