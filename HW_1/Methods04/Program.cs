﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Найти расстояние между двумя точками в двумерном декартовом пространстве.

namespace Methods04
{
    class Program
    {
        static void Main(string[] args)
        {
            Point A = new Point();
            Point B = new Point();
            Console.WriteLine("Enter point A(x,y) coordinates:");
            Console.Write("x: ");
            A.X = Double.Parse(Console.ReadLine());
            Console.Write("y: ");
            A.Y = Double.Parse(Console.ReadLine());
            Console.WriteLine("Enter point B(x,y) coordinates:");
            Console.Write("x: ");
            B.X = Double.Parse(Console.ReadLine());
            Console.Write("y: ");
            B.Y = Double.Parse(Console.ReadLine());

            Console.WriteLine($"Distance between points A & B: {CalculateDistance(A,B)}");
            Console.ReadKey();
        }

        static double  CalculateDistance(Point A, Point B)
        {
            double distance = Math.Sqrt(Math.Pow((A.X - B.X), 2) + Math.Pow((A.Y - B.Y), 2));
            return distance;
        }
    }
}
