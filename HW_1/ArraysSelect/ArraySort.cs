﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysSelect
{
    public class ArraySort
    {
        public void SelectSort(int[] array)
        {
            if (array == null)
            {
                throw new NullReferenceException();
            }
            if (array.Length <= 1)
            {
                return;
            }

            for (int i = 0; i < array.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[j] < array[min])
                    {
                        min = j;
                    }
                }

                if (i != min)
                {
                    int temp = array[i];
                    array[i] = array[min];
                    array[min] = temp;
                }
            }
        }
    }
}
