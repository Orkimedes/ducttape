﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Найти корень натурального числа с точностью до целого (рассмотреть вариант последовательного подбора и метод бинарного поиска)

namespace Cycles03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number:");
            int number = Int32.Parse(Console.ReadLine());
            Console.WriteLine($"Root = {CalculateSquareRoot(1, number)}");
            Console.WriteLine($"Root Binary = {CalculateSquareRootBinary(number)}");
            Console.ReadKey();
        }

        static int CalculateSquareRoot(int low, int number)
        {
            int root = 0;
            int difference = number;
            int currentDifference;

            for (int i = low; i <= number; i++)
            {
                //Console.WriteLine(i);
                currentDifference = Math.Abs(number - i * i);
                if (currentDifference < difference)
                {
                    root = i;
                    difference = currentDifference;
                }
                else break;
            }
            return root;
        }

        static int CalculateSquareRootBinary(int number)
        {

            int left = 1;
            int right = number;

            while (left < right)
            {
                int middle = (right + left) / 2;
                Console.WriteLine($"middle = {middle}");
                Console.WriteLine(left * left);
                Console.WriteLine(middle*middle);
                Console.WriteLine(right*right);

                if (middle * middle == number)
                {
                    return middle;
                }
                else if (middle*middle< number)
                {
                    left = middle+1;
                }
                else
                {
                    right = middle-1;
                }
            }

            return left;
        }
    }
}
