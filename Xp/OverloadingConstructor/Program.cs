﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverloadingConstructor
{
    class Program
    {
        static void Main(string[] args)
        {
            Pixel a, b, c;
            int x, y;
            byte color;

            a = new Pixel(255);
            b = new Pixel(25, 66, 192);
            b.ReturnParams(out x, out y, out color);
            c = new Pixel(x, y, color);

            a.DisplayParams();
            b.DisplayParams();
            c.DisplayParams();

            Console.ReadLine();

        }

        class Pixel
        {
            int x, y;
            byte color;

            public Pixel(byte color)
            {
                x = y = 0;
                Console.WriteLine("one");
                this.color = color;
            }

            public Pixel(int x, int y, byte color) : this(color)
            {
                Console.WriteLine("two");
                this.x = x;
                this.y = y;
            }

            
            public void ReturnParams(out int x, out int y, out byte color)
            {
                x = this.x;
                y = this.y;
                color = this.color;
            }

            public void DisplayParams()
            {
                Console.WriteLine($"Pixel coordinates({x}.{y}, color:{color})");
            }
        }
    }
}
