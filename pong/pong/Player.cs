﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Pong
{
    public class Player
    {
        const int MOVEMENT_SPEED = 3;
        public bool isUpPressed, isDownPressed;
        public int score = 0;
        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
                scoreLabel.Text = score.ToString();
            }
        }
        public PictureBox paddle;
        private bool? wasGoingUpLastTick;
        private int numberOfTicksContiniousMovement;
        private int upperLimit;
        private int bottomLimit;
        private Label scoreLabel;


        public Player(PictureBox _paddle, Label _label, Point topOfWorld, Point bottomOfWorld)
        {
            paddle = _paddle;
            scoreLabel = _label;
            upperLimit = topOfWorld.Y;
            bottomLimit = bottomOfWorld.Y - paddle.Height;
        }

        internal void ProcessMove()
        {
            bool? paddleGoingUp = null;

            if (isUpPressed)
            {
                paddleGoingUp = true;
            }

            if (isDownPressed)
            {
                if (paddleGoingUp.HasValue)
                {
                    paddleGoingUp = null;
                }
                else
                {
                    paddleGoingUp = false;
                }
            }

            if (wasGoingUpLastTick.HasValue)
            {
                if (!paddleGoingUp.HasValue)
                {
                    wasGoingUpLastTick = null;
                    numberOfTicksContiniousMovement = 0;
                }
                else if (wasGoingUpLastTick.Value == paddleGoingUp.Value)
                {
                    numberOfTicksContiniousMovement++;
                }
                else
                {
                    wasGoingUpLastTick = paddleGoingUp;
                    numberOfTicksContiniousMovement = 1;
                }
            }
            else if (paddleGoingUp.HasValue)
            {
                wasGoingUpLastTick = paddleGoingUp;
                numberOfTicksContiniousMovement = 1;
            }

            DoMove(paddleGoingUp);
        }

        private void DoMove(bool? paddleGoingUp)
        {
            if (paddleGoingUp.HasValue)
            {
                var speed = MOVEMENT_SPEED + numberOfTicksContiniousMovement;
                if (paddleGoingUp.Value)
                {
                    speed *= -1;
                }

                paddle.Location = new Point(paddle.Location.X, Math.Max(upperLimit, Math.Min(bottomLimit, paddle.Location.Y + speed)));
            }
        }
    }
}
