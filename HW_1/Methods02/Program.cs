﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Вводим число (0-999), получаем строку с прописью числа.
//Для задания 2 расширить диапазон до 999 миллиардов

namespace Methods02
{
    class Program
    {
        static string[] units = { "", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        static string[] decades = { "", "", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто" };
        static string[] hundreds = { "", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};

        static void Main(string[] args)
        {
            Console.Write("Enter number(0-999):");
            int inputNumber = Int32.Parse(Console.ReadLine());
            Console.WriteLine(ConvertNumberToText(inputNumber));
            Console.ReadKey();
        }

        static string ConvertNumberToText(int number)
        {

            int level100, level10, level1;
            string text, textPart1, textPart2, textPart3;
            if (number == 0)
            {
                return "ноль";
            }

            level100 = number / 100;
            level10 = (number % 100) / 10;
            level1 = number % 10;

            if (level100 > 0)
            {
                textPart1 = hundreds[level100] + " ";
            }
            else
            {
                textPart1 = "";
            }

            if (level10 > 1)
            {
                textPart2 = decades[level10] + " ";
                textPart3 = units[level1];
            }
            else if(level10 == 1)
            {
                textPart2 = "";
                textPart3 = units[level1 + 10];
            }
            else
            {
                textPart2 = "";
                textPart3 = units[level1];
            }

            text = textPart1 + textPart2 + textPart3;

            return text;
        }

    }
}
