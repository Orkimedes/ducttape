﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectList
{
    class OList : IList
    {
        private object[] container;
        public object this[int index]
        {
            get
            {
                return container[index];
            }
            set
            {
                container[index] = value;
            }
        }

        public bool IsReadOnly => false;//можно реализовать через конструктор
        public bool IsFixedSize => false;//можно реализовать через конструктор
        public bool IsSynchronized => false; //какая-то хрень связанная с потокобезопасностью


        public int Count { get; private set; } = 0;
        public int Capacity { get; private set; } = 0;
        public OList()
        {
            container = new object[0];//перенести
        }

        public object SyncRoot => throw new NotImplementedException();//какая-то хрень связанная с потокобезопасностью


        public int Add(object value)
        {
            ExtendContainer();
            container.SetValue(value, Count);
            return Count++;
        }

        public void Clear()
        {
            for (int i = 0; i < Count; i++)
            {
                container[i] = null;
            }
            Count = 0;
        }

        public bool Contains(object value)
        {
            var position = IndexOf(value);
            if (position == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void CopyTo(Array array, int index)
        {
            if (array.Rank == 1)
            {
                if (array != null)
                {
                    if ((array.Length - index) >= Count)
                    {
                        container.CopyTo(array, index);
                    }
                }
            }
        }

        public IEnumerator GetEnumerator()
        {
            IEnumerator enumerator = new OListEnumerator(container, Count);
            return enumerator;
        }

        public int IndexOf(object value)
        {
            if (value == null)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (container[i] == null)
                    {
                        return i;
                    }
                    
                }
            }
            else
            {
                for (int i = 0; i < Count; i++)
                {
                    if (value.Equals(container[i]))
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public void Insert(int index, object value)
        {
            ExtendContainer();
            for (int i = Count - 1; i > index; i--)
            {
                container[i] = container[i + 1];
            }
            container.SetValue(value, index);
            Count++;
        }

        private void ExtendContainer()
        {
            if (Count == 0)
            {
                Array.Resize(ref container, 1);
                Capacity = container.Length;
            }

            if (Capacity == Count)
            {
                Array.Resize(ref container, Count * 2);//test
                Capacity = container.Length;
            }
        }

        public void Remove(object value)
        {
            var position = IndexOf(value);
            if (position != -1)
            {
                RemoveAt(position);
            }
        }

        public void RemoveAt(int index)
        {
            for (int i = index; i < (Count - 2); i++)
            {
                container[i] = container[i + 1];
            }
            container[Count - 1] = null;
            Count--;
        }
    }
}
