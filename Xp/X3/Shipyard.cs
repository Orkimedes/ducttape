﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X3
{
    public class Shipyard
    {
        public Ship CreateShip<T>()
        {
            Transport<T> ship = new Transport<T>();
            return ship;
        }

        public Ship CreateShip()
        {
            Transport ship = new Transport();
            return ship;
        }
    }
}
