﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X3
{
    interface ICarrier<T>
    {
        void Add(T item);
        void Remove(T item);
    }
}
