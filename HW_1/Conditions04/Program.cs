﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Посчитать выражение макс(а*б*с, а+б+с)+3

namespace Conditions04
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, result;
            Console.WriteLine("Enter three variables:");
            Console.Write("A: ");
            a = Int32.Parse(Console.ReadLine());
            Console.Write("B: ");
            b = Int32.Parse(Console.ReadLine());
            Console.Write("C: ");
            c = Int32.Parse(Console.ReadLine());
            result = Calculate(a, b, c);
            Console.WriteLine($"Result: {result}");
            Console.ReadKey();
        }

        static int Calculate(int a, int b, int c)
        {
            int result, sum, mult;
            mult = a * b * c;
            sum = a + b + c;
            if (mult > sum)
            {
                result = mult + 3;
            }
            else
            {
                result = sum + 3;
            }


            return result;
        }
    }
}
