﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ship USS00 = new Ship(); - невозможно создать экземпляр абстрактного класса
            Ship USS01 = new Transport<Cargo.Goods>();
            Transport<Cargo.Goods> USS02 = new Transport<Cargo.Goods>();
            USS01.Move();//Move Transport - реализция производного класса
            //USS01.AddPassenger(); - не сработает, так как ссылка на объект имеет тип базового класса и методы производного класса недоступны
            USS02.Move();
            USS02.Stop();
            IPassengerCarrier USS03 = new Transport();
            USS03.AddPassenger();// можно вызвать лишь методы, описанные в интерфейсе
            Transport USS04 = USS03 as Transport;
            USS04.Move(); //преобразование к типу Transport при помощи ключевого слова as
            Ship G01 = new Transport<Cargo.Goods>();
            
            Console.ReadLine();
        }

        
    }



}
