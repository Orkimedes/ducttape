﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{

    class MyClass
    {
        public MyClass(int a)
        {
            field = a;
        }
        private int field;

        public void Display()
        {
            Console.WriteLine(field);
        }
    }
    public struct MyStruct
    {
        public MyStruct(int a)
        {
            field = a;
        }
        private int field;

        public void Display()
        {
            Console.WriteLine(field);
        }
    }


    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("ValueTypes");
            int a = 15;
            int b = 15;
            if (a == b)
            {
                Console.WriteLine("== true");
            }
            if (a.Equals(b))
            {
                Console.WriteLine("Equals b true");
            }
            if (Equals(a,b))
            {
                Console.WriteLine("Equals a b true");
            }
            Console.WriteLine(a.GetHashCode());
            Console.WriteLine(b.GetHashCode());

            Console.WriteLine("strings time!");

            Test("someString"); //true
            string x = "someString";
            string y = "someString";
            char c = 'c';
            string z = c.ToString();
            Test(z);//false
            string w = x + y;
            Test(w);
            string v = "a" + "b";
            Test(v);

            if (x == y)
            {
                Console.WriteLine("== true");
            }
            if (x.Equals(y))
            {
                Console.WriteLine("Equals y true");
            }
            if (Equals(x, y))
            {
                Console.WriteLine("Equals x y true");
            }
            Console.WriteLine(x.GetHashCode());
            Console.WriteLine(y.GetHashCode());


            Console.WriteLine("objects!!!");
            var obj1 = new MyClass(42);
            var obj2 = obj1;
            if (obj1 == obj2)
            {
                Console.WriteLine("== true");
            }
            if (obj1.Equals(obj2))
            {
                Console.WriteLine("Equals y true");
            }
            if (Equals(obj1, obj2))
            {
                Console.WriteLine("Equals x y true");
            }
            Console.WriteLine("objects 2");

            var obj3 = new MyClass(111);
            var obj4 = new MyClass(111);
            if (obj3 == obj4)
            {
                Console.WriteLine("== true");
            }
            if (obj3.Equals(obj4))
            {
                Console.WriteLine("Equals y true");
            }
            if (Equals(obj3, obj4))
            {
                Console.WriteLine("Equals x y true");
            }
            else
            {
                Console.WriteLine("false");
            }

            Console.WriteLine("null");
            object nstring2 = null;// и string, и object дают одинаковые результаты
            object nstring1 = null; 

            if(nstring1 == nstring2)
            {
                Console.WriteLine("== true");
            }
            /*if (nstring1.Equals(nstring2))
            {
                Console.WriteLine("Equals y true");
            }*/ //-ex
            if (Equals(nstring1, nstring2))
            {
                Console.WriteLine("Equals x y true");
            }
            else
            {
                Console.WriteLine("false");
            }

            Console.WriteLine("structs");
            var su1 = new MyStruct(25);
            var su2 = new MyStruct(25);

            /*if (su1 == su2)
            {
                Console.WriteLine("== true");
            } - для своих структур нужно переопределить*/
            if (su1.Equals(su2))
            {
                Console.WriteLine("Equals y true");
            }
            if (Equals(su1, su2))
            {
                Console.WriteLine("Equals x y true");
            }
            else
            {
                Console.WriteLine("false");
            }
            Console.WriteLine(su1.GetHashCode());
            Console.WriteLine(su2.GetHashCode());


            Console.ReadLine();
        }



        public static void Test(string str)
        {
            Console.Write("The string,");
            string strInterned = string.IsInterned(str); //- проверяет была ли интернирован string и возвращает ссылку на него если он есть, или null если нет
            if (strInterned == null)
                Console.WriteLine("'{0}', is not interned.", str);
            else
                Console.WriteLine("'{0}', is interned.", strInterned);
        }
    }
}