﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankInheritance
{
    class Student : Person
    {
        private int[] testScores;

       
        public Student(string firstName, string lastName, int identification, int[] scores):base(firstName,lastName,identification)
        {
            testScores = scores;
        }

        public char Calculate()
        {
            int sum = 0;
            int average;
            char result;
            for (int i = 0; i < testScores.Length; i++)
            {
                sum += testScores[i];
            }
            average = sum / testScores.Length;

            if (average >= 90 && average <= 100)
            {
                result = 'O';
            }
            else if(average >=80)
            {
                result = 'E';
            }
            else if (average >=70)
            {
                result = 'A';
            }
            else if (average >= 55)
            {
                result = 'P';
            }
            else if (average >= 40)
            {
                result = 'D';
            }
            else
            {
                result = 'T';
            }
            return result;
        }
        
    }
}
