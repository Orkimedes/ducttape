﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например, задано число 123, вывести 321.

namespace Cycles06
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Number");
            int number = Int32.Parse(Console.ReadLine());
            int reversedNumber = ReverseNumber(number);
            Console.WriteLine($"{reversedNumber}");
            Console.ReadKey();
        }

        static int ReverseNumber(int number)
        {
            int result = 0;
            do
            {
                result = 10 * result + (number % 10);
                number /= 10;
            }
            while (number > 0);

            return result;
        }
    }
}
