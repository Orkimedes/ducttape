﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CalculateCondition_2and3_result6()
        {
            var result = Conditions01.Program.CalculateCondition(2, 3);

            Assert.AreEqual(result, 6);
        }

        [TestMethod]
        public void CalculateCondition_3and2_result5()
        {
            var result = Conditions01.Program.CalculateCondition(3, 2);

            Assert.AreEqual(result, 5);
        }

        [TestMethod]
        public void CalculateCondition_minus3and3_result0()
        {
            var result = Conditions01.Program.CalculateCondition(-3, 3);

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void CalculateCondition_minus6and4_result24()
        {
            var result = Conditions01.Program.CalculateCondition(-6, 4);

            Assert.AreEqual(result, -24);
        }




    }
}
