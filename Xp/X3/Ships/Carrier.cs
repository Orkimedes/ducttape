﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X3
{
    class Carrier<T>:Ship, ICarrier<T> where T:IWingman 
    {
        public Carrier() : base()
        {

        }

        public void Add(T item)
        {
            throw new NotImplementedException();
        }

        public override void Move()
        {
            Console.WriteLine("Carrier is moving");
        }

        public void Remove(T item)
        {
            throw new NotImplementedException();
        }
    }
}
