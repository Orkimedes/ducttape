﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverloadingIndexer
{
    class Program
    {
        static void Main(string[] args)
        {
            var myArray = new SomeIndexer(100);
            myArray[99] = 25;
            Console.WriteLine(myArray[0]);
            Console.WriteLine(myArray['c']);//25
            var myCoolFcrtl = new FacIndexer();
            Console.WriteLine(myCoolFcrtl[3]);//6
            Console.ReadLine();
        }

    }

    public class SomeIndexer
    {
        int[] internalArray;
        readonly int Size;

        public SomeIndexer(int size)
        {
            internalArray = new int[size];
            Size = size;
        }
        public int this[int indexer]
        {
            get
            {
                if (indexer < Size)
                {
                    return internalArray[indexer];
                }
                else
                {
                    return 0;
                }
                
            }

            set
            {
                if (indexer < Size)
                {
                    internalArray[indexer] = value;
                }
            }
        }

        public int this[char indexer]
        {
            get
            {
                int i = indexer;
                if (indexer < Size)
                {
                    return internalArray[indexer];
                }
                else
                {
                    return 0;
                }
            }
        }
    }
    public class FacIndexer
    {
        public int this[int indexer]
        {
            get
            {
                return Fctrl(indexer);
            }
        }

        private int Fctrl(int x)
        {
            if (x == 0)
            {
                return 1;
            }
            else
            {
                return x * Fctrl(x - 1);
            }
        }


    }
}
