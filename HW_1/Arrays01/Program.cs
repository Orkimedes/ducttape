﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Найти минимальный элемент массива
Найти максимальный элемент массива
Найти индекс минимального элемента массива
Найти индекс максимального элемента массива
Посчитать сумму элементов массива с нечетными индексами
Сделать реверс массива (массив в обратном направлении)
Посчитать количество нечетных элементов массива
Поменять местами первую и вторую половину массива, например, для массива 1 2 3 4, результат 3 4 1 2
*/

namespace Arrays01
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[] initialArray, reversedArray, shiftedArray;
            int arraySize;
            Console.Write("Enter array size: ");
            arraySize = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Source array:");
            initialArray = CreateRandomArray(arraySize);
            DisplayArray(initialArray);

            Console.WriteLine("Inverted array");
            reversedArray = ReverseArray(initialArray);
            DisplayArray(reversedArray);

            Console.WriteLine("Shifted array:");
            shiftedArray = ShiftArray(initialArray);
            DisplayArray(shiftedArray);

            Console.WriteLine($"Max value of array: {ReturnArrayMax(initialArray)}");

            Console.WriteLine($"Max value's index: {ReturnArrayMaxIndex(initialArray)}");

            Console.WriteLine($"Min value of array: {ReturnArrayMin(initialArray)}");

            Console.WriteLine($"Min value's index: {ReturnArrayMinIndex(initialArray)}");

            Console.WriteLine($"Quantity of odd values: {CalculateOddQuantity(initialArray)}");

            Console.WriteLine($"Sum of odd values: {CalculateOddValuesSum(initialArray)}");

            Console.ReadKey();
        }

        public static int[] CreateRandomArray(int arraySize)
        {
            int[] array = new int[arraySize];
            Random randomNumber = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = randomNumber.Next(100);
            }
            return array;
        }

        public static int[] ReverseArray(int[] inputArray)
        {
            int[] outputArray = new int[inputArray.Length];
            for (int i = 0, j = inputArray.Length - 1; i < inputArray.Length; i++, j--)
            {
                outputArray[j] = inputArray[i];
            }
            return outputArray;
        }

        public static void DisplayArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"{array[i]}");
                if (i < array.Length - 1)
                {
                    Console.Write(",");
                }
            }
            Console.WriteLine();
        }

        public static int[] ShiftArray(int[] inputArray)
        {
            int[] outputArray = new int[inputArray.Length];
            Array.Copy(inputArray, outputArray, inputArray.Length);
            int j = (int)Math.Ceiling((double)inputArray.Length / 2);
            for (int i = 0; j < inputArray.Length; i++,j++)
            {
                int tempNumber = outputArray[i];
                outputArray[i] = outputArray[j];
                outputArray[j] = tempNumber;
            }
            return outputArray;
        }

        public static int ReturnArrayMax(int[] inputArray)
        {
            int max = inputArray[0];
            for (int i = 1; i < inputArray.Length; i++)
            {
                if (max < inputArray[i])
                {
                    max = inputArray[i];
                }
            }
            return max;
        }

        public static int ReturnArrayMin(int[] inputArray)
        {
            int min = inputArray[0];
            for (int i = 1; i < inputArray.Length; i++)
            {
                if (min > inputArray[i])
                {
                    min = inputArray[i];
                }
            }
            return min;
        }

        public static int ReturnArrayMaxIndex(int[] inputArray)
        {
            int max = inputArray[0];
            int index = 0;
            for (int i = 1; i < inputArray.Length; i++)
            {
                if (max < inputArray[i])
                {
                    max = inputArray[i];
                    index = i;
                }
            }
            return index;
        }

        public static int ReturnArrayMinIndex(int[] inputArray)
        {
            int min = inputArray[0];
            int index = 0;
            for (int i = 1; i < inputArray.Length; i++)
            {
                if (min > inputArray[i])
                {
                    min = inputArray[i];
                    index = i;
                }
            }
            return index;
        }

        public static int CalculateOddValuesSum(int[] inputArray)
        {
            int sum = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                if ((inputArray[i] & 1) == 1)
                {
                    sum += inputArray[i];
                }
            }
            return sum;
        }

        public static int CalculateOddQuantity(int[] inputArray)
        {
            int quantity = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                if ((inputArray[i] & 1) == 1)
                {
                    quantity ++;
                }
            }
            return quantity;
        }
    }
}
