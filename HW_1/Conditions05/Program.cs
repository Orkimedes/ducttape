﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Написать программу определения оценки студента по его рейтингу, на основе следующих правил
 
Рейтинг Оценка
0-19 F
20-39 E
40-59 D
60-74 C
75-89 B
90-100 A
*/

namespace Conditions05
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            int rate;
            char evaluation;
            Console.Write("Enter Student's Name: ");
            name = Console.ReadLine();
            Console.Write("Enter Student's Rate: ");
            rate = Int32.Parse(Console.ReadLine());
            evaluation = CalculateEvaluation(rate);
            Console.WriteLine($"Evaluation of student {name} is {evaluation}");
            Console.ReadKey();
        }

        static char CalculateEvaluation(int rate)
        {
            char evalution;
            if (rate < 20)
                evalution = 'F';
            else if (rate < 40)
                evalution = 'E';
            else if (rate < 60)
                evalution = 'D';
            else if (rate < 75)
                evalution = 'C';
            else if (rate < 90)
                evalution = 'B';
            else
                evalution = 'A';
            return evalution;
        }
    }
}
