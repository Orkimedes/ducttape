﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankExceptions
{
    class Solution
    {

        static void Main(String[] args)
        {
            string S = Console.ReadLine();
            int number;
            try
            {
                number = Int32.Parse(S);
                Console.WriteLine(number);
            }
            catch (FormatException)
            {
                Console.WriteLine("Bad String");
            }
            Console.ReadKey();
        }
    }
}
