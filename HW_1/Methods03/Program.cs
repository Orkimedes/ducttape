﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число
//Для задания 3 расширить диапазон до 999 миллиардов 

namespace Methods03
{
    class Program
    {
        static string[] units = { "", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять",
                                  "одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" };
        static string[] decades = { "", "", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто" };
        static string[] hundreds = { "", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот" };

        static void Main(string[] args)
        {
            Console.Write("Enter number:");
            string inputText = Console.ReadLine();
            Console.WriteLine(ConvertToNumber(inputText));
            Console.ReadKey();
        }

        static int ConvertToNumber(string inputText)
        {
            int resultNumber = 0;
            string[] numbers = inputText.Split();
            for (int i = 0; i < numbers.Length; i++)
            {
                for (int j = 0; j < hundreds.Length; j++)
                {
                    if (hundreds[j] == numbers[i])
                    {
                        resultNumber += j * 100;
                    }
                }

                for (int j = 0; j < decades.Length; j++)
                {
                    if (decades[j] == numbers[i])
                    {
                        resultNumber += j * 10;
                    }
                }

                for (int j = 0; j < units.Length; j++)
                {
                    if (units[j] == numbers[i])
                    {
                        resultNumber += j;
                    }
                }
            }
            return resultNumber;
        }
    }
}
