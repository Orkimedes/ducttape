﻿namespace Pong
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.paddleObject1 = new System.Windows.Forms.PictureBox();
            this.paddleObject2 = new System.Windows.Forms.PictureBox();
            this.ballObject = new System.Windows.Forms.PictureBox();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.labelPlayerOneObject = new System.Windows.Forms.Label();
            this.labelPlayerTwoObject = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.paddleObject1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paddleObject2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ballObject)).BeginInit();
            this.SuspendLayout();
            // 
            // paddleObject1
            // 
            this.paddleObject1.Image = ((System.Drawing.Image)(resources.GetObject("paddleObject1.Image")));
            this.paddleObject1.Location = new System.Drawing.Point(12, 133);
            this.paddleObject1.Name = "paddleObject1";
            this.paddleObject1.Size = new System.Drawing.Size(20, 200);
            this.paddleObject1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.paddleObject1.TabIndex = 0;
            this.paddleObject1.TabStop = false;
            // 
            // paddleObject2
            // 
            this.paddleObject2.Image = ((System.Drawing.Image)(resources.GetObject("paddleObject2.Image")));
            this.paddleObject2.Location = new System.Drawing.Point(1007, 133);
            this.paddleObject2.Name = "paddleObject2";
            this.paddleObject2.Size = new System.Drawing.Size(20, 200);
            this.paddleObject2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.paddleObject2.TabIndex = 1;
            this.paddleObject2.TabStop = false;
            // 
            // ballObject
            // 
            this.ballObject.Image = ((System.Drawing.Image)(resources.GetObject("ballObject.Image")));
            this.ballObject.Location = new System.Drawing.Point(169, 86);
            this.ballObject.Name = "ballObject";
            this.ballObject.Size = new System.Drawing.Size(20, 20);
            this.ballObject.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.ballObject.TabIndex = 2;
            this.ballObject.TabStop = false;
            // 
            // Timer
            // 
            this.Timer.Enabled = true;
            this.Timer.Interval = 10;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("TodaySHOP-Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(456, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 38);
            this.label1.TabIndex = 3;
            this.label1.Text = "Score";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelPlayerOneObject
            // 
            this.labelPlayerOneObject.AutoSize = true;
            this.labelPlayerOneObject.BackColor = System.Drawing.Color.Transparent;
            this.labelPlayerOneObject.Font = new System.Drawing.Font("TodaySHOP-Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayerOneObject.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelPlayerOneObject.Location = new System.Drawing.Point(456, 47);
            this.labelPlayerOneObject.Name = "labelPlayerOneObject";
            this.labelPlayerOneObject.Size = new System.Drawing.Size(37, 38);
            this.labelPlayerOneObject.TabIndex = 4;
            this.labelPlayerOneObject.Text = "0";
            this.labelPlayerOneObject.Click += new System.EventHandler(this.label2_Click);
            // 
            // labelPlayerTwoObject
            // 
            this.labelPlayerTwoObject.AutoSize = true;
            this.labelPlayerTwoObject.BackColor = System.Drawing.Color.Transparent;
            this.labelPlayerTwoObject.Font = new System.Drawing.Font("TodaySHOP-Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayerTwoObject.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelPlayerTwoObject.Location = new System.Drawing.Point(520, 47);
            this.labelPlayerTwoObject.Name = "labelPlayerTwoObject";
            this.labelPlayerTwoObject.Size = new System.Drawing.Size(37, 38);
            this.labelPlayerTwoObject.TabIndex = 5;
            this.labelPlayerTwoObject.Text = "0";
            this.labelPlayerTwoObject.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("TodaySHOP-Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(492, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 38);
            this.label2.TabIndex = 6;
            this.label2.Text = ":";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1039, 624);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelPlayerTwoObject);
            this.Controls.Add(this.labelPlayerOneObject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ballObject);
            this.Controls.Add(this.paddleObject2);
            this.Controls.Add(this.paddleObject1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Game";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pong";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Game_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Game_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.paddleObject1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paddleObject2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ballObject)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox paddleObject1;
        private System.Windows.Forms.PictureBox paddleObject2;
        private System.Windows.Forms.PictureBox ballObject;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelPlayerOneObject;
        private System.Windows.Forms.Label labelPlayerTwoObject;
        private System.Windows.Forms.Label label2;
    }
}

