﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KustomList
{
    class KList<T> : IList<T>,IList
    {
        public int Count { get; private set; } = 0;
        public int Capacity { get; private set; } = 0;
        public bool IsReadOnly => false;//для readonly коллекций
        private T[] container;
        public T this[int index] { get => container[index]; set => container[index] = value; }
        //private KEnumenator<T> enumenator;

        public KList()
        {
            container = new T[0];
        }
        public void Add(T item)
        {
            ExtendContainer();
            container.SetValue(item, Count);
            Count++;
        }
        public void Clear()
        {
            for (int i = 0; i < Capacity; i++)
            {
                container[i] = default;
            }
            Count = 0;
        }
        public void RemoveAt(int index)
        {
            for (int i = index; i < (Count - 2); i++)
            {
                container[i] = container[i + 1];
            }
            container[Count - 1] = default;
            Count--;
        }


        public int IndexOf(T item)
        {
            if (item == null)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (container[i] == null)
                    {
                        return i;
                    }

                }
            }
            else
            {
                for (int i = 0; i < Capacity; i++)
                {
                    if (item.Equals(container[i]))
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            ExtendContainer();
            for (int i = Count - 1; i > index; i--)
            {
                container[i] = container[i + 1];
            }
            container.SetValue(item, index);
            Count++;
        }

        public bool Contains(T item)
        {
            var position = IndexOf(item);
            if (position == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array.Rank == 1)
            {
                if (array != null)
                {
                    if ((array.Length - arrayIndex) >= Count)
                    {
                        container.CopyTo(array, arrayIndex);
                    }
                }
            }
        }

        public bool Remove(T item)
        {
            var position = IndexOf(item);
            if (position != -1)
            {
                RemoveAt(position);
                return true;
            }
            else
            {
                return false;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            IEnumerator enumerator = new KListEnumerator<T>(container, Count);
            return enumerator;
        }
        private void ExtendContainer()
        {
            if (Count == 0)
            {
                Array.Resize(ref container, 1);
                Capacity = container.Length;
            }

            if (Capacity == Count)
            {
                Array.Resize(ref container, Count * 2);//test
                Capacity = container.Length;
            }
        }


        public IEnumerator<T> GetEnumerator()
        {
            IEnumerator<T> enumerator = new KListEnumerator<T>(container, Count);
            return enumerator;
        }


    }
        /*foreach (Foo bar in baz)
    {
       ...
    }

    it's functionally equivalent to writing:

    IEnumerator bat = baz.GetEnumerator();
    while (bat.MoveNext())
    {
       bar = (Foo)bat.Current
       ...
    }*/
        class KListEnumerator<T> : IEnumerator, IEnumerator<T>
        {
            private int position = -1;
            private int amount;
            private T[] enumArray;
            public KListEnumerator(T[] container, int amount)
            {
                this.amount = amount;
                enumArray = container;
            }
            public bool MoveNext()
            {
                if (position < amount - 1)
                {
                    position++;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            public object Current => enumArray[position];
            T IEnumerator<T>.Current => enumArray[position];

            public void Reset()

            {
                position = -1;
            }

            public void Dispose()
            {

            }
        }
    }

