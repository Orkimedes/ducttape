﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KustomList
{
    class Program
    {
        static void Main(string[] args)
        {
            var intList = new KList<int>();
            intList.Add(5);
            intList.Add(6);
            intList.Add(7);
            intList.RemoveAt(2);
            intList.Add(666);
            intList.Add(42);
            intList.Add(1001010);
            intList.Add(123);
            foreach (int item in intList)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            intList.RemoveAt(1);
            intList.RemoveAt(1);
            foreach (int item in intList)
            {
                Console.WriteLine(item);
            }
            var t = (IEnumerable<int>)intList;
            Console.WriteLine();
            var jsdhkjsah = intList[0] + intList[1];
            foreach (var item in t)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();

        }
    }
}
