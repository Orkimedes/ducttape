﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankRecursion
{
    class Solution
    {

        static int Factorial(int n)
        {
            int result = 1;
            if (n > 1)
            {
                result = n;
            }
            n = n - 1;
            if (n > 1)
            {
                result = result * Factorial(n);
            }
            return result;
        }

        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int result = Factorial(n);
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
