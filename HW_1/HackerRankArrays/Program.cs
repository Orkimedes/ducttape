﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankArrays
{
    class Solution
    {

        static void Main(String[] args)
        {
            int maxHourGlass = int.MinValue;
            int[][] arr = new int[6][];
            for (int arr_i = 0; arr_i < 6; arr_i++)
            {
                string[] arr_temp = Console.ReadLine().Split(' ');
                arr[arr_i] = Array.ConvertAll(arr_temp, Int32.Parse);
                
            }
            for (int y = 0; y < arr.Length - 2; y++)
            {
                for (int x = 0; x < arr[y].Length; x++)
                {
                    int temp = CalculateHourGlassSum(arr, x, y);
                    if (temp > maxHourGlass)
                    {
                        maxHourGlass = temp;
                    }
                }
            }
            Console.WriteLine(maxHourGlass);
        }

        static int CalculateHourGlassSum(int[][] array, int x, int y)
        {
            int sum = array[x][y] + array[x][y+1] + array[x][y+2] + array[x + 1][y + 1] + array[x+2][y] + array[x + 2][y + 1] + array[x + 2][y + 2];

            return sum;
        }
    }
}
