﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILTest
{
    class D
    {
        public int Number; 
        public int Value { get; set; } = 10; //австосвойство, инициализируется в конструкторе по умолчанию 
        public D(int number)
        {
            Number = number;
        }
    }
}
