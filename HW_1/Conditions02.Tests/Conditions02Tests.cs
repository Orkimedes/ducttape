﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Conditions02.Tests
{
    [TestClass]
    public class Conditions02Tests
    {
        [TestMethod]
        public void ZeroCoordinatesThrowsArgumentExeption()
        {
            Assert.ThrowsException<ArgumentException>(() => Conditions02.Program.ReturnPointQuarter(0,0));
        }

        [TestMethod]
        public void ReturnFirstQuarter()
        {
            //Arrange
            int x = 1;
            int y = 10;
            int expected = 1;
            //Act
            int actual = Conditions02.Program.ReturnPointQuarter(x, y);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnSecondQuarter()
        {
            //Arrange
            int x = -1;
            int y = 10;
            int expected = 2;
            //Act
            int actual = Conditions02.Program.ReturnPointQuarter(x, y);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnThirdQuarter()
        {
            //Arrange
            int x = -51;
            int y = -10;
            int expected = 3;
            //Act
            int actual = Conditions02.Program.ReturnPointQuarter(x, y);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void ReturnFourthQuarter()
        {
            //Arrange
            int x = 10;
            int y = -10;
            int expected = 4;
            //Act
            int actual = Conditions02.Program.ReturnPointQuarter(x, y);
            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
