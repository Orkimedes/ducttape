﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleCalculator
{
    public partial class MainScreen : Form
    {
        private int a;
        private int b;
        private int result;

        private enum OperationState

        {
            empty,
            add,
            substract,
            multiply,
            divide,
        }
        private OperationState currentOperation;

        public MainScreen()
        {
            InitializeComponent();
        }

        private void MainScreen_Load(object sender, EventArgs e)
        {
            SetDefaults();
        }

        private void ButtonPlus_Click(object sender, EventArgs e)
        {
            currentOperation = OperationState.add;
            TextOutOperation.Text = ButtonPlus.Text;
        }

        private void ButtonMinus_Click(object sender, EventArgs e)
        {
            currentOperation = OperationState.substract;
            TextOutOperation.Text = ButtonMinus.Text;
        }

        private void TextOutOperation_Click(object sender, EventArgs e)
        {
            
        }

        private void ButtonMultiply_Click(object sender, EventArgs e)
        {
            currentOperation = OperationState.multiply;
            TextOutOperation.Text = ButtonMultiply.Text;
        }

        private void ButtonDivide_Click(object sender, EventArgs e)
        {
            currentOperation = OperationState.divide;
            TextOutOperation.Text = ButtonDivide.Text;
        }

        private void ButtonCalculate_Click(object sender, EventArgs e)
        {
            bool success = false;
            switch (currentOperation)
            {
                case OperationState.empty:
                    InfoTextOut.Text = "No operation";
                    break;
                case OperationState.add:
                    result = a + b;
                    success = true;
                    break;
                case OperationState.substract:
                    result = a - b;
                    success = true;
                    break;
                case OperationState.multiply:
                    result = a * b;
                    success = true;
                    break;
                case OperationState.divide:
                    if (b == 0)
                    {
                        InfoTextOut.Text = "divide by zero";
                    }
                    else
                    {
                        result = a / b;
                        success = true;
                    }

                    break;
            }
            if (success)
            {
                TextBoxOutRes.Text = result.ToString();
            }
            
            currentOperation = OperationState.empty;

        }

        private void InputTextA_TextChanged(object sender, EventArgs e)
        {
            if (InputTextA.Text != "")
                a = Convert.ToInt32(InputTextA.Text);
        }

        private void InputTextB_TextChanged(object sender, EventArgs e)
        {
            if (InputTextB.Text != "")
                b = Convert.ToInt32(InputTextB.Text);
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            SetDefaults();
        }

        private void SetDefaults()
        {
            InputTextA.Text = null;
            InputTextB.Text = null;
            TextBoxOutRes.Text = null;
            TextOutOperation.Text = null;
            InfoTextOut.Text = null;
            currentOperation = OperationState.empty;
        }
    }

        
}
