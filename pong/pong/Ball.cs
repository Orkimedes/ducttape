﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pong
{
    public class Ball
    {
        private PictureBox ball;
        private int ballSpeedX = 5;
        private int ballSpeedY = 5;
        private Point topOfWorld;
        private Point bottomOfWorld;
        private Point startPosition;
        private Random rand = new Random();
        private Player player1;
        private Player player2;



        public Ball(PictureBox ball, Point topOfWorld, Point bottomOfWorld, Point startPosition, Player player1, Player player2)
        {
            this.ball = ball;
            this.topOfWorld = topOfWorld;
            this.bottomOfWorld = new Point(bottomOfWorld.X - ball.Width,bottomOfWorld.Y - ball.Height);
            this.startPosition = startPosition;
            ball.Location = startPosition;
            this.player1 = player1;
            this.player2 = player2;
        }

        public void ProcessMove()
        {
            if (ball.Location.Y == topOfWorld.Y || ball.Location.Y == bottomOfWorld.Y)
            {
                ballSpeedY = -ballSpeedY;
            }

            if (ball.Location.X == topOfWorld.X)
            {
                Score(player2);
            }

            else if (ball.Location.X == bottomOfWorld.X)
            {
                Score(player1);
            }

            if (player1.paddle.Bounds.IntersectsWith(ball.Bounds) || player2.paddle.Bounds.IntersectsWith(ball.Bounds))
            {
                ball.Location = new Point(ball.Location.X - ballSpeedX, ball.Location.Y);
                ballSpeedX = -ballSpeedX;
            }
            ball.Location = new Point(Math.Max(topOfWorld.X, Math.Min(bottomOfWorld.X, ball.Location.X + ballSpeedX)), Math.Max(topOfWorld.Y, Math.Min(bottomOfWorld.Y, ball.Location.Y + ballSpeedY)));
            

        }

        private void Score(Player winningPlayer)
        {
            winningPlayer.Score++;
            ball.Location = startPosition;
            ResetSpeed();
        }

        private void ResetSpeed()
        {
            do
            {
                ballSpeedX = rand.Next(-5, 5);
                ballSpeedY = rand.Next(-5, 5);
            } while (Math.Abs(ballSpeedX)<=3 ||Math.Abs(ballSpeedY)<=3);
        }
    }
}
