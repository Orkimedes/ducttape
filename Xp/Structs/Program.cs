﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structs
{
    class Program
    {
        static void Main(string[] args)
        {
            User Vasya;
            Vasya.name = "Vasya";
            Vasya.age = 34;
            Vasya.DisplayInfo();

            User Petya;
            //Petya.DisplayInfo(); - поля не инициализированы
            //int x = Petya.age; - поля не инициализированы
            Console.ReadKey();

            User Ignat = new User();
            Ignat.DisplayInfo();// по умолчанию
        }
    }

    struct User
    {
        public string name;
        public int age;

        public void DisplayInfo()
        {
            Console.WriteLine($"Name: {name}  Age: {age}");
        }
    }
}
