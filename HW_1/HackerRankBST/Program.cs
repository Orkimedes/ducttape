﻿using System;

namespace HackerRankBST
{
    class Program
    {
        static int getHeight(Node root)
        {
            int height = 0;
            int heightLeft = 0;
            int  heightRight = 0;
            if (root.left != null)
            {
                heightLeft++;
                heightLeft += getHeight(root.left);
            }
            if (root.right != null)
            {
                heightRight++;
                heightRight += getHeight(root.right);
            }
            if (heightLeft > heightRight)
            {
                height = heightLeft;
            }
            else
            {
                height = heightRight;
            }
            return height;
        }

        static Node insert(Node root, int data)
        {
            if (root == null)
            {
                return new Node(data);
            }
            else
            {
                Node cur;
                if (data <= root.data)
                {
                    cur = insert(root.left, data);
                    root.left = cur;
                }
                else
                {
                    cur = insert(root.right, data);
                    root.right = cur;
                }
                return root;
            }
        }
        static void Main(String[] args)
        {
            Node root = null;
            int T = Int32.Parse(Console.ReadLine());
            while (T-- > 0)
            {
                int data = Int32.Parse(Console.ReadLine());
                root = insert(root, data);
            }
            int height = getHeight(root);
            Console.WriteLine(height);

        }

        
        
    }

    class Node
    {
        public Node left, right;
        public int data;
        public Node(int data)
        {
            this.data = data;
            left = right = null;
        }
    }

}
