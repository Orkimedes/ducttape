﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Посчитать сумму цифр заданного числа

namespace Cycles05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Number");
            int number = Int32.Parse(Console.ReadLine());
            int sum = CalculateSumOfNumbers(number);
            Console.WriteLine($"{sum}");
            Console.ReadKey();
        }

        static int CalculateSumOfNumbers(int number)
        {
            int sum = 0;
            do
            {
                int numeral = 0;
                numeral = number % 10;
                number = number / 10;
                sum += numeral;
            }
            while (number > 0);

            return sum;
        }



    }
}
