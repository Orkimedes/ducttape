﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysBubble
{
    public class ArraySort
    {
        public void BubbleSort(int[] array)
        {
            if (array == null)
            {
                throw new NullReferenceException();
            }
            if (array.Length <= 1)
            {
                return;
            }
            bool flag;
            do
            {
                flag = false;
                for (int i = 0; i < (array.Length - 1); i++)
                {
                    if (array[i + 1] < array[i])
                    {
                        flag = true;
                        int temp = array[i + 1];
                        array[i + 1] = array[i];
                        array[i] = temp;
                    }
                }
            } while (flag);
        }
    }
}
