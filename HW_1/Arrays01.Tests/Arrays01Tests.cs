﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Arrays01.Tests
{
    [TestClass]
    public class Arrays01Tests
    {
        [TestMethod]
        public void ReverseArrayOneElement()
        {
            //Arrange
            int[] array = { 666 };
            int[] expected = { 666 };
            //Act
            int[] actual = Arrays01.Program.ReverseArray(array);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReverseArrayPositiveNumbers()
        {
            //Arrange
            int[] array = { 5, 1, 2, 3 };
            int[] expected = { 3, 2, 1, 5 };
            //Act
            int[] actual = Arrays01.Program.ReverseArray(array);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReverseArrayNegativeNumbers()
        {
            //Arrange
            int[] array = { -5, 1, -2, 3 };
            int[] expected = { 3, -2, 1, -5 };
            //Act
            int[] actual = Arrays01.Program.ReverseArray(array);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShiftArrayPositiveNumbers()
        {
            //Arrange
            int[] array = { 5, 1, 2, 3 };
            int[] expected = { 2, 3, 5, 1 };
            //Act
            int[] actual = Arrays01.Program.ShiftArray(array);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShiftArrayNegativeNumbers()
        {
            //Arrange
            int[] array = { -5, 1, -2, 3 };
            int[] expected = { -2, 3, -5, 1 };
            //Act
            int[] actual = Arrays01.Program.ShiftArray(array);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShiftArrayOddNumberOfElements()
        {
            //Arrange
            int[] array = { 5, 1, 0, 2, 3 };
            int[] expected = { 2, 3, 0, 5, 1 };
            //Act
            int[] actual = Arrays01.Program.ShiftArray(array);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShiftArrayOneElement()
        {
            //Arrange
            int[] array = { 666 };
            int[] expected = { 666 };
            //Act
            int[] actual = Arrays01.Program.ShiftArray(array);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMaxPositive()
        {
            //Arrange
            int[] array = { 5, 1, 0, 2, 3 };
            int expected = 5;
            //Act
            int actual = Arrays01.Program.ReturnArrayMax(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMaxNegative()
        {
            //Arrange
            int[] array = { -5, -1, 0, -2, -3 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMax(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMaxEqualElements()
        {
            //Arrange
            int[] array = { 6, 6, 6, 3, 3, 3 };
            int expected = 6;
            //Act
            int actual = Arrays01.Program.ReturnArrayMax(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMaxZeroes()
        {
            //Arrange
            int[] array = { 0, 0, 0, 0, 0, 0 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMax(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMinPositive()
        {
            //Arrange
            int[] array = { 5, 1, 0, 2, 3 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMin(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMinNegative()
        {
            //Arrange
            int[] array = { -5, -1, 0, -2, -3 };
            int expected = -5;
            //Act
            int actual = Arrays01.Program.ReturnArrayMin(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMinEqualElements()
        {
            //Arrange
            int[] array = { 6, 6, 6, 3, 3, 3 };
            int expected = 3;
            //Act
            int actual = Arrays01.Program.ReturnArrayMin(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMinZeroes()
        {
            //Arrange
            int[] array = { 0, 0, 0, 0, 0, 0 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMin(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        //ReturnArrayMaxIndex
        [TestMethod]
        public void ReturnArrayMaxIndexPositive()
        {
            //Arrange
            int[] array = { 5, 1, 0, 2, 3 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMaxIndex(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMaxIndexNegative()
        {
            //Arrange
            int[] array = { -5, -1, 0, -2, -3 };
            int expected = 2;
            //Act
            int actual = Arrays01.Program.ReturnArrayMaxIndex(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMaxIndexEqualElements()
        {
            //Arrange
            int[] array = { 6, 6, 6, 3, 3, 3 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMaxIndex(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMaxIndexZeroes()
        {
            //Arrange
            int[] array = { 0, 0, 0, 0, 0, 0 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMaxIndex(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        //ReturnArrayMinIndex

        [TestMethod]
        public void ReturnArrayMinIndexPositive()
        {
            //Arrange
            int[] array = { 5, 1, 0, 2, 3 };
            int expected = 2;
            //Act
            int actual = Arrays01.Program.ReturnArrayMinIndex(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMinIndexNegative()
        {
            //Arrange
            int[] array = { -5, -1, 0, -2, -3 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMinIndex(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMinIndexEqualElements()
        {
            //Arrange
            int[] array = { 6, 6, 6, 3, 3, 3 };
            int expected = 3;
            //Act
            int actual = Arrays01.Program.ReturnArrayMinIndex(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnArrayMinIndexZeroes()
        {
            //Arrange
            int[] array = { 0, 0, 0, 0, 0, 0 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.ReturnArrayMinIndex(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }


        //CalculateOddValuesSum
        [TestMethod]
        public void CalculateOddValuesSumNormal()
        {
            //Arrange
            int[] array = { 5, 1, 0, 2, 3 };
            int expected = 9;
            //Act
            int actual = Arrays01.Program.CalculateOddValuesSum(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CalculateOddValuesSum_AllEven_Result0()
        {
            //Arrange
            int[] array = { 6, 2, 0, 8, 4 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.CalculateOddValuesSum(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void CalculateOddValuesSumNegativeNumbers()
        {
            //Arrange
            int[] array = { -5, -1, 0, -2, 3 };
            int expected = -3;
            //Act
            int actual = Arrays01.Program.CalculateOddValuesSum(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void CalculateOddValuesSumAllZeroes()
        {
            //Arrange
            int[] array = { 0, 0, 0, 0, 0 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.CalculateOddValuesSum(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void CalculateOddValuesSumOneElement()
        {
            //Arrange
            int[] array = { 667 };
            int expected = 667;
            //Act
            int actual = Arrays01.Program.CalculateOddValuesSum(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }


        //CalculateOddQuantity
        [TestMethod]
        public void CalculateOddQuantityNormal()
        {
            //Arrange
            int[] array = { 5, 1, 0, 2, 3 };
            int expected = 3;
            //Act
            int actual = Arrays01.Program.CalculateOddQuantity(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CalculateOddQuantity_AllEven_Result0()
        {
            //Arrange
            int[] array = { 6, 2, 0, 8, 4 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.CalculateOddQuantity(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void CalculateOddQuantityNegativeNumbers()
        {
            //Arrange
            int[] array = { -5, -1, 0, -2, 3 };
            int expected = 3;
            //Act
            int actual = Arrays01.Program.CalculateOddQuantity(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void CalculateOddQuantityAllZeroes()
        {
            //Arrange
            int[] array = { 0, 0, 0, 0, 0 };
            int expected = 0;
            //Act
            int actual = Arrays01.Program.CalculateOddQuantity(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void CalculateOddQuantityOneElement()
        {
            //Arrange
            int[] array = { 667 };
            int expected = 1;
            //Act
            int actual = Arrays01.Program.CalculateOddQuantity(array);
            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
