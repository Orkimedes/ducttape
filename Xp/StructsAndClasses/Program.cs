﻿using System;

class Program
{
    private static void Main(string[] args)
        {
            MyStruct su1 = new MyStruct(); 
            MyStruct su2 = new MyStruct();
            su2.x = 1;
            su2.y = 2;
            su1 = su2;
            su2.x = 5; // su1.x=1 по-прежнему
            Console.WriteLine(su1.x); // 1
            Console.WriteLine(su2.x); // 5

            MyClass cls1 = new MyClass(); 
            MyClass cls2 = new MyClass();
            cls2.x = 1;
            cls2.y = 4;
            cls1 = cls2;
            cls2.x = 7; // теперь и cls1.x = 7, так как обе ссылки и cls1 и cls2 
                        // указывают на один объект в хипе
        Console.WriteLine(cls1.x); // 7
            Console.WriteLine(cls2.x); // 7
        }
}

struct MyStruct
{
    public int x;
    public int y;
    public MyClass cls;
}
class MyClass
{
    public int x;
    public int y;
}