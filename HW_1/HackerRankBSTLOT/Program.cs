﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
class Node
{
    public Node left, right;
    public int data;
    public Node(int data)
    {
        this.data = data;
        left = right = null;
    }
}
class Solution
{

    static void levelOrder(Node root)
    {
        Queue<Node> numbers = new Queue<Node>();
        Node currentNode;
        if (root != null)
        {
            numbers.Enqueue(root);
        }
        while (numbers.Count > 0)
        {
            currentNode = numbers.Dequeue();
            if (currentNode.left != null)
            {
                numbers.Enqueue(currentNode.left);
            }
            if (currentNode.right != null)
            {
                numbers.Enqueue(currentNode.right);
            }
            Console.Write($"{currentNode.data}"+" ");

        }
    }

    static Node insert(Node root, int data)
    {
        if (root == null)
        {
            return new Node(data);
        }
        else
        {
            Node cur;
            if (data <= root.data)
            {
                cur = insert(root.left, data);
                root.left = cur;
            }
            else
            {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }
    static void Main(String[] args)
    {
        Node root = null;
        int T = Int32.Parse(Console.ReadLine());
        while (T-- > 0)
        {
            int data = Int32.Parse(Console.ReadLine());
            root = insert(root, data);
        }
        levelOrder(root);

    }
}