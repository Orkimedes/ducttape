﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions01
{
    public class Program
    {
        static void Main(string[] args)
        {
            int a, b, result;

            Console.WriteLine("Enter a:");
            a = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter b:");
            b = Int32.Parse(Console.ReadLine());
            result = CalculateCondition(a, b);
            Console.WriteLine($"Calculation Result: {result }");
            Console.ReadKey();

        }

        public static int CalculateCondition(int a, int b)
        {
            int result;
            if (a%2 == 0)
            {
                result = a * b;
            }
            else
            {
                result = a + b;
            }
            return result;
        }
    }
}
