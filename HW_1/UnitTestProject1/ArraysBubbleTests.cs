﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class ArraysBubbleTests
    {

        [TestMethod]
        public void BubbleSortThrowsNullException()
        {
            //Arrange
            var arraySort = new ArraysBubble.ArraySort();
            int[] array = null;
            //Act -> Assert
            Assert.ThrowsException<NullReferenceException>(() => arraySort.BubbleSort(array));
        }


        [TestMethod]
        public void BubbleSort_9534_3459()
        {
            //Arrange
            var arraySort = new ArraysBubble.ArraySort();
            int[] array = { 9, 5, 3, 4 };
            int[] expected = { 3, 4, 5, 9 };
            //Act
            arraySort.BubbleSort(array);
            //Assert
            CollectionAssert.AreEqual(array, expected);
        }

        [TestMethod]
        public void BubbleSort_9999_9999()
        {
            //Arrange
            var arraySort = new ArraysBubble.ArraySort();
            int[] array = { 9, 9, 9, 9 };
            int[] expected = { 9, 9, 9, 9 };
            //Act
            arraySort.BubbleSort(array);
            //Act -> Assert
            CollectionAssert.AreEqual(array, expected);
        }

        [TestMethod]
        public void BubbleSort_1_1()
        {
            //Arrange
            var arraySort = new ArraysBubble.ArraySort();
            int[] array = { 1 };
            int[] expected = { 1 };
            //Act
            arraySort.BubbleSort(array);
            //Act -> Assert
            CollectionAssert.AreEqual(array, expected);
        }

        [TestMethod]
        public void BubbleSort_negative()
        {
            //Arrange
            var arraySort = new ArraysBubble.ArraySort();
            int[] array = { -10, -500, 0, -87, -6 };
            int[] expected = { -500, -87, -10, -6, 0 };
            //Act
            arraySort.BubbleSort(array);
            //Act -> Assert
            CollectionAssert.AreEqual(array, expected);
        }
    }
}
