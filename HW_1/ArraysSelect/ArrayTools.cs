﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysSelect
{
    public static class ArrayTools
    {
        public static int[] CreateRandomArray(int arraySize)
        {
            int[] array = new int[arraySize];
            Random randomNumber = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = randomNumber.Next(100);
            }
            return array;
        }

        public static void DisplayArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"{array[i]}");
                if (i < array.Length - 1)
                {
                    Console.Write(",");
                }
            }
            Console.WriteLine();
        }
    }
}
