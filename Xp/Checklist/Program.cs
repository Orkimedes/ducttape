﻿using System;

namespace Checklist
{
    struct Person
    {
        public string name;
        public int age;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person person;
            person.name = "Bob";
            Console.WriteLine(person.name);
            Console.ReadLine();

        }
    }
}
