﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Вычислить факториал числа n. n! = 1*2*…*n-1*n;!

namespace Cycles04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number to calculate factorial:");
            int number = Int32.Parse(Console.ReadLine());
            Console.WriteLine($"{number}! = {Factorial(number)}");
            Console.ReadKey();
        }

        static int Factorial(int number)
        {
            int result = number;
            if (result > 1)
            {
                number--;
                result *= Factorial(number);
            }
            return result;
        }
    }
}
