﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Определить какой четверти принадлежит точка с координатами (х,у)


namespace Conditions02
{
    public class  Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            int x, y, result;

            Console.WriteLine("Enter Point A(x,y) Coordinates");
            Console.WriteLine("Enter x:");
            x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter y:");
            y = Int32.Parse(Console.ReadLine());

            result = ReturnPointQuarter(x, y);
            switch (result)
            {
                case 1:
                    Console.WriteLine("Point is located on first quarter");
                    break;
                case 2:
                    Console.WriteLine("Point is located on second quarter");
                    break;
                case 3:
                    Console.WriteLine("Point is located on third quarter");
                    break;
                case 4:
                    Console.WriteLine("Point is located on fourth quarter");
                    break;
                default:
                    Console.WriteLine("Point is located on coordinate axis");
                    break;
            }

            Console.ReadKey();
        }

        public static short ReturnPointQuarter(int x, int y)
        {
            short quarter = 0;

            if (x == 0 || y ==0)
            {
                throw new ArgumentException();
            }
            if (x > 0)
            {
                if (y > 0)
                {
                    quarter = 1;
                }

                else quarter = 4;
            }

            else if (x < 0)
            {
                if (y < 0)
                {
                    quarter = 3;
                }

                else quarter = 2;
            }

            return quarter;
        }
    }
}
