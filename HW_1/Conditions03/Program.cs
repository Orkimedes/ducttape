﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Найти сумму только положительных из трех чисел

namespace Conditions03
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, sum;
            Console.WriteLine("Enter three variables:");
            Console.Write("A: ");
            a = Int32.Parse(Console.ReadLine());
            Console.Write("B: ");
            b = Int32.Parse(Console.ReadLine());
            Console.Write("C: ");
            c = Int32.Parse(Console.ReadLine());
            sum = CalculatePositiveNumbers(a, b, c);
            Console.WriteLine($"Sum of positive numbers: {sum}");
            Console.ReadKey();
        }

        static int CalculatePositiveNumbers(int a, int b, int c)
        {
            int sum = 0;
            int[] array = new int[] { a, b, c };
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > 0)
                {
                    sum += array[i];
                }
            }
            return sum;
        }
    }
}
