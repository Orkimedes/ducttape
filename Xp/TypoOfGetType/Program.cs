﻿using System;

class Program
{
    public static void Main()
    {
        Dog spot = new Dog();
        PrintTypes(spot);
        Console.WriteLine(typeof(Dog));
        Console.WriteLine(spot.GetType());
        Console.WriteLine(spot.GetType().GetType());
        Console.WriteLine(spot.GetType().GetType().GetType());

        void PrintTypes(Animal animal)
        {
            Console.WriteLine(animal.GetType() == typeof(Animal)); // false, типы Dog и Animal не совпадают
            Console.WriteLine(animal is Animal);                   // true, так как типы Dog находится в иерархии Animal
            Console.WriteLine(animal.GetType() == typeof(Dog));    // true
            Console.WriteLine(animal is Dog);                      // true 
            /*There is a strong semantic difference between the two:
                The equality == checks on type equality: in other words, if A : B than the equality test will fail for A.GetType() == typeof(B) whereas A is B will succeed.
                If the object is null, it will throw a System.NullReferenceException. In the second case, it will return false.
            */
            
        }

        Console.ReadLine();
    }

    class Animal { }
    class Dog : Animal { }

    

    
}