﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverloadingOp
{
    class Program
    {
        static void Main(string[] args)
        {
            var cell1 = new MemoryCell { Value = 1 };
            Console.WriteLine(cell1); //OverloadingOp.MemoryCell, Value = 1
            if (cell1)
            {
                Console.WriteLine("not a zero");//not a zero
            }
            var cell2 = cell1 + 2;

            int someInteger = 10;
            Console.WriteLine(cell2);//3
            someInteger = (int)cell1;
            Console.WriteLine(someInteger);//3
            MemoryCell cell3;
            //Console.WriteLine($"c2V2 = {c2.Value}"); - экземпляр класса не cоздан
            cell3 = new MemoryCell { Value = 3 };
            if (cell2.Equals(cell3))
            {
                Console.WriteLine("cell2 equals cell3");
            }
            if (cell2.Equals(cell1))
            {
                Console.WriteLine("cell2 equals cell1");
            }
            else
            {
                Console.WriteLine("cell2 not equals cell1");
            }



            
            Console.ReadLine();
        }

    }

    public class MemoryCell
    {
        public int Value { get; set; }// = 10; //австосвойство, инициализируется в конструкторе по умолчанию 
        //public int Value2 => 10;// свойство только для чтения, значение инициализируется сразу же
        //public int Value3;//поле, инициализируется в конструкторе по умолчанию 
        public MemoryCell()
        {
            Console.WriteLine("base ctor");
        }
        public override int GetHashCode()
        {
            return Value;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)// если второй объект ссылается на null, то объекты не равны, нужна для того чтобы в случае null не брать для него getType и не пытаться получить значение поля Value
            {
                return false;
            }
            if (ReferenceEquals(this, obj))//если ссылки одинаковы, то объекты равны
            {
                return true;
            }
            if (this.GetType() != obj.GetType() )// если тип различается, то объекты не равны
            {
                return false;
            }
            if (this.Value == (obj as MemoryCell).Value)// приводим объект к memoryCell, чтобы получить доступ к полю
            {
                return true;
            }
            else
            {
                return false;
            }

               


        }

        public static MemoryCell operator +(MemoryCell m1, MemoryCell m2)
        {
            return new MemoryCell { Value = m1.Value + m2.Value };
        }
        public static MemoryCell operator -(MemoryCell m1, MemoryCell m2)
        {
            return new MemoryCell { Value = m1.Value - m2.Value };
        }
        public static bool operator >(MemoryCell m1, MemoryCell m2)
        {
            return m1.Value > m2.Value;
        }
        public static bool operator ==(MemoryCell m1, MemoryCell m2)
        {
            return m1.Value == m2.Value;
        }
        public static bool operator !=(MemoryCell m1, MemoryCell m2)
        {
            return m1.Value != m2.Value;
        }

        public static bool operator  <(MemoryCell m1, MemoryCell m2)
        {
            return m1.Value < m2.Value;
        }
        public static MemoryCell operator +(MemoryCell m1, int a)
        {
            return new MemoryCell { Value = m1.Value + a };
        }

        public static MemoryCell operator -(MemoryCell m1, int a)
        {
            return new MemoryCell { Value = m1.Value - a };
        }

        public static MemoryCell operator ++(MemoryCell m1)
        {
            return new MemoryCell { Value = m1.Value + 1 };
        }
        public static bool operator true(MemoryCell m1)
        {
            if (m1.Value == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool operator false(MemoryCell m1)
        {
            if (m1.Value == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static explicit operator int(MemoryCell m1)
        {
            return m1.Value;
        }

        public override string ToString()
        {
            return base.ToString() + ", Value = " + Value.ToString();
        }
        

    }

    public class ChildMemoryCell : MemoryCell
    {
        public string Name { get; set; }
       
    }



}
