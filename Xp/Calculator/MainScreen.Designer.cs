﻿namespace SimpleCalculator
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InfoTextOut = new System.Windows.Forms.Label();
            this.InputTextA = new System.Windows.Forms.TextBox();
            this.ButtonPlus = new System.Windows.Forms.Button();
            this.ButtonMinus = new System.Windows.Forms.Button();
            this.ButtonMultiply = new System.Windows.Forms.Button();
            this.ButtonDivide = new System.Windows.Forms.Button();
            this.ButtonCalculate = new System.Windows.Forms.Button();
            this.InputTextB = new System.Windows.Forms.TextBox();
            this.TextBoxOutRes = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextOutOperation = new System.Windows.Forms.Label();
            this.ButtonClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // InfoTextOut
            // 
            this.InfoTextOut.AutoSize = true;
            this.InfoTextOut.BackColor = System.Drawing.Color.LightBlue;
            this.InfoTextOut.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.InfoTextOut.Location = new System.Drawing.Point(15, 258);
            this.InfoTextOut.Name = "InfoTextOut";
            this.InfoTextOut.Size = new System.Drawing.Size(24, 13);
            this.InfoTextOut.TabIndex = 1;
            this.InfoTextOut.Text = "info";
            // 
            // InputTextA
            // 
            this.InputTextA.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InputTextA.Location = new System.Drawing.Point(15, 38);
            this.InputTextA.Name = "InputTextA";
            this.InputTextA.Size = new System.Drawing.Size(327, 38);
            this.InputTextA.TabIndex = 3;
            this.InputTextA.TextChanged += new System.EventHandler(this.InputTextA_TextChanged);
            // 
            // ButtonPlus
            // 
            this.ButtonPlus.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ButtonPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonPlus.Location = new System.Drawing.Point(365, 38);
            this.ButtonPlus.Name = "ButtonPlus";
            this.ButtonPlus.Size = new System.Drawing.Size(80, 65);
            this.ButtonPlus.TabIndex = 9;
            this.ButtonPlus.Text = "+";
            this.ButtonPlus.UseVisualStyleBackColor = false;
            this.ButtonPlus.Click += new System.EventHandler(this.ButtonPlus_Click);
            // 
            // ButtonMinus
            // 
            this.ButtonMinus.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ButtonMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold);
            this.ButtonMinus.Location = new System.Drawing.Point(449, 38);
            this.ButtonMinus.Name = "ButtonMinus";
            this.ButtonMinus.Size = new System.Drawing.Size(80, 65);
            this.ButtonMinus.TabIndex = 10;
            this.ButtonMinus.Text = "-";
            this.ButtonMinus.UseVisualStyleBackColor = false;
            this.ButtonMinus.Click += new System.EventHandler(this.ButtonMinus_Click);
            // 
            // ButtonMultiply
            // 
            this.ButtonMultiply.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ButtonMultiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold);
            this.ButtonMultiply.Location = new System.Drawing.Point(365, 109);
            this.ButtonMultiply.Name = "ButtonMultiply";
            this.ButtonMultiply.Size = new System.Drawing.Size(80, 65);
            this.ButtonMultiply.TabIndex = 11;
            this.ButtonMultiply.Text = "*";
            this.ButtonMultiply.UseVisualStyleBackColor = false;
            this.ButtonMultiply.Click += new System.EventHandler(this.ButtonMultiply_Click);
            // 
            // ButtonDivide
            // 
            this.ButtonDivide.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ButtonDivide.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold);
            this.ButtonDivide.Location = new System.Drawing.Point(449, 109);
            this.ButtonDivide.Name = "ButtonDivide";
            this.ButtonDivide.Size = new System.Drawing.Size(80, 65);
            this.ButtonDivide.TabIndex = 12;
            this.ButtonDivide.Text = "/";
            this.ButtonDivide.UseVisualStyleBackColor = false;
            this.ButtonDivide.Click += new System.EventHandler(this.ButtonDivide_Click);
            // 
            // ButtonCalculate
            // 
            this.ButtonCalculate.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ButtonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold);
            this.ButtonCalculate.Location = new System.Drawing.Point(365, 191);
            this.ButtonCalculate.Name = "ButtonCalculate";
            this.ButtonCalculate.Size = new System.Drawing.Size(80, 65);
            this.ButtonCalculate.TabIndex = 13;
            this.ButtonCalculate.Text = "=";
            this.ButtonCalculate.UseVisualStyleBackColor = false;
            this.ButtonCalculate.Click += new System.EventHandler(this.ButtonCalculate_Click);
            // 
            // InputTextB
            // 
            this.InputTextB.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InputTextB.Location = new System.Drawing.Point(15, 109);
            this.InputTextB.Name = "InputTextB";
            this.InputTextB.Size = new System.Drawing.Size(327, 38);
            this.InputTextB.TabIndex = 14;
            this.InputTextB.TextChanged += new System.EventHandler(this.InputTextB_TextChanged);
            // 
            // TextBoxOutRes
            // 
            this.TextBoxOutRes.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBoxOutRes.Location = new System.Drawing.Point(12, 211);
            this.TextBoxOutRes.Name = "TextBoxOutRes";
            this.TextBoxOutRes.Size = new System.Drawing.Size(327, 38);
            this.TextBoxOutRes.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LightBlue;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 31);
            this.label2.TabIndex = 16;
            this.label2.Text = "Result";
            // 
            // TextOutOperation
            // 
            this.TextOutOperation.AutoSize = true;
            this.TextOutOperation.BackColor = System.Drawing.Color.LightBlue;
            this.TextOutOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextOutOperation.Location = new System.Drawing.Point(137, 80);
            this.TextOutOperation.Name = "TextOutOperation";
            this.TextOutOperation.Size = new System.Drawing.Size(89, 24);
            this.TextOutOperation.TabIndex = 17;
            this.TextOutOperation.Text = "operation";
            this.TextOutOperation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TextOutOperation.Click += new System.EventHandler(this.TextOutOperation_Click);
            // 
            // ButtonClear
            // 
            this.ButtonClear.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ButtonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold);
            this.ButtonClear.Location = new System.Drawing.Point(449, 191);
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.Size = new System.Drawing.Size(80, 65);
            this.ButtonClear.TabIndex = 18;
            this.ButtonClear.Text = "C";
            this.ButtonClear.UseVisualStyleBackColor = false;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(606, 280);
            this.Controls.Add(this.ButtonClear);
            this.Controls.Add(this.TextOutOperation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextBoxOutRes);
            this.Controls.Add(this.InputTextB);
            this.Controls.Add(this.ButtonCalculate);
            this.Controls.Add(this.ButtonDivide);
            this.Controls.Add(this.ButtonMultiply);
            this.Controls.Add(this.ButtonMinus);
            this.Controls.Add(this.ButtonPlus);
            this.Controls.Add(this.InputTextA);
            this.Controls.Add(this.InfoTextOut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.MainScreen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label InfoTextOut;
        private System.Windows.Forms.TextBox InputTextA;
        private System.Windows.Forms.Button ButtonPlus;
        private System.Windows.Forms.Button ButtonMinus;
        private System.Windows.Forms.Button ButtonMultiply;
        private System.Windows.Forms.Button ButtonDivide;
        private System.Windows.Forms.Button ButtonCalculate;
        private System.Windows.Forms.TextBox InputTextB;
        private System.Windows.Forms.TextBox TextBoxOutRes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label TextOutOperation;
        private System.Windows.Forms.Button ButtonClear;
    }
}

